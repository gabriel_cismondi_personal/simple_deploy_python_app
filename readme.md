# SIMPLE APP DEPLOY WITH ANSIBLE

The script runs on ubuntu server, before beginning should be changed the ansible host ip in hosts.ini and the User and
IdentityFile line in ssh.cfg (in order to use user and password remove the comment after the ip in host.ini and comment
User and IdentityFile in ssh.cfg)

Run the playbook from the main directory by providing the env variables as shown below:

    ansible-playbook playbooks/deploy-app.yml -e 'redis_host=.....' -e 'env=......'
    
If no variable is provided it will take the default values